---
title: "Pitch"
author: "Antsje van der Leij"
date: "February 4, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## snelle pich van Antsje van der Leij

### favoriete onderwerpen

Algeritmen en data strukturen is een mooi onderwerp. Ik vind het leuk om met BLAST te werken en met algaritmen bezig te zijn. Het ontwerpen van databasen is ook altijd een leuk project om me bezig te gaan. Bij de biologie kant lijk het me interesand om met micro-organismen of planten te werken. Qua progameertalen heb ik ervaring met Java, Python, SQL en R.

## projecten

MetaPhlAn is een computationeel hulpmiddel voor het profileren van de samenstelling van microbiële gemeenschappen (Bacteria, Archaea, Eukaryotes en Virussen) van metagenomische shotgun-sequencinggegevens met soortniveau.

Met de metaphlan2 community profiler kan de gebruiker de sequencinggegevens uploaden en worden de resultaten om gezet naar een dynamische output

https://bitbucket.org/aavanderleij/metaphlan2_community_profiler/src/master/